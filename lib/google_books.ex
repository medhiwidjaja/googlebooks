defmodule GoogleBooks do
  

  @moduledoc """

    Elixir client for the Google Books API. 

      iex> GoogleBooks.search "The Little Prince"

  """
  alias GoogleBooks.Query

  def start(_opts \\ []) do
    Query.start_link(fn -> %{} end)
  end

  def search(query, opts \\ []) do
    Query.volumes(query, opts)
  end

end
