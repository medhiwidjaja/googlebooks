defmodule GoogleBooks.Query do
  use Agent
  alias GoogleBooks.Config

  @moduledoc """
    Default configuration values for gravity.
  """
  @name __MODULE__
  @base_uri "www.googleapis.com/books/v1"
  @type query :: String.t
  @type url :: String.t
  @type options :: List.t 

  def start_link(_opts) do
    HTTPoison.start
    Agent.start_link(fn -> %{} end, name: @name)
  end

  def get(key) do
    Agent.get(@name, &Map.get(&1, key))
  end

  def volumes(query, _opts \\ []) do
    results = Config.protocol() <> "://" <> @base_uri <> "/volumes?q=" <> query
      |> HTTPoison.get!
      |> Map.get(:body)
      |> Poison.decode!
    Agent.update(@name, fn _ -> results end)
  end
end