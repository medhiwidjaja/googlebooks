defmodule GoogleBooksTest do
  use ExUnit.Case, async: true

  test "stores search results using Agent" do
    {:ok, _} = GoogleBooks.start([])
    assert GoogleBooks.Query.get("totalItems") == nil

    GoogleBooks.search("steelheart")
    assert is_integer(GoogleBooks.Query.get("totalItems"))
  end
end
