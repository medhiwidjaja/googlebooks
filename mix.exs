defmodule GoogleBooks.Mixfile do
  use Mix.Project

  def project do
    [app: :googlebooks,
     version: "1.0.0",
     elixir: "~> 1.2",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     description: description(),
     package: package(),
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:dialyxir, "~> 0.3.5", only: [:dev]},
      {:ex_doc, "~> 0.13", only: :dev},
      {:earmark, "~> 1.0.1", only: :dev},
      {:credo, "~> 0.4", only: [:dev, :test]},
      {:httpoison, "~> 1.2"},
      {:poison, "~> 3.1"}
    ]
  end

  defp description do
      """
          Elixir client for the Google Books API.
      """
  end

  defp package do
      [
          files: ["lib", "mix.exs", "README.md"],
          maintainers: ["Medhi Widjaja"],
          licenses: ["MIT"],
          links: %{"GitHub" => "https://github.com/medhiwidjaja/googlebooks",
          "Docs" => "http://hexdocs.pm/googlebooks/"}
      ]
  end
end
