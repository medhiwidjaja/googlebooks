# Google Books

Elixir client for the Google Books API.

## Installation

Add googlebooks to your list of dependencies in `mix.exs`:

```
        def deps do
          [{:googlebooks, "~> 1.0.0"}]
        end
```

## Usage

```
    iex> GoogleBooks.search("The Little Prince")

```

## Configuration

In your `mix.exs`:

```
config :googlebooks,
        secure: false # true by default (https), set it to false to use http

```

**googlebooks** © 2018, Medhi Widjaja. Released under the [MIT] License.<br>
Authored and maintained by Medhi Widjaja.

> GitHub [@medhiwidjaja](https://github.com/medhiwidjaja)

[MIT]: http://mit-license.org/
[contributors]: http://github.com/medhiwidjaja/googlebooks/contributors
